import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { UsersComponent } from "./users.component";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";

describe("UsersComponent", () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [UsersComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("checks the row count", () => {
    expect(component.rows).toBeLessThanOrEqual(5);
  });

  it("get the first count", () => {
    expect(component.getPageData().length).toBeLessThanOrEqual(5);
  });
});
