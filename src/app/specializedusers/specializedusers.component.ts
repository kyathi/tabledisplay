import { Component, OnInit } from "@angular/core";
import { UsersComponent } from "../users/users.component";
import { UserProfileService } from "../user-profile.service";

@Component({
  selector: "app-specializedusers",
  templateUrl: "./specializedusers.component.html",
  styleUrls: ["./specializedusers.component.css"]
})
export class SpecializedusersComponent extends UsersComponent {
  constructor(private userService1: UserProfileService) {
    super(userService1);
  }

  ngOnInit(): void {
    this.rows = -1;
    super.ngOnInit();
  }
}
