import { Component, OnInit } from "@angular/core";
import { user } from "../Models/user";
import { UserProfileService } from "../user-profile.service";

@Component({
  selector: "app-heroes",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.css"],
  providers: [UserProfileService]
})
export class UsersComponent implements OnInit {
  constructor(private userService: UserProfileService) {}

  public users: user[] = [];
  public displayUsers: user[] = [];
  public rows: number = 5;
  public pageNo: number = 1;
  public maxPages: number = 1;

  ngOnInit(): void {
    this.users = this.userService.getUserData();
    if (this.rows > 0) {
      this.maxPages = Math.ceil(this.users.length / this.rows);
      if (this.maxPages > 1) {
        this.displayUsers = this.getPageData();
      } else {
        this.displayUsers = this.users;
      }
    } else {
      this.displayUsers = this.users;
    }
  }

  onSave(user: user): void {
    console.log(this.rows);
    this.userService.postStatusForUser(user.id, user.status);
  }

  getPageData(): user[] {
    return this.users.slice(
      (this.pageNo - 1) * this.rows,
      this.rows * this.pageNo
    );
  }

  onNextPage(): void {
    if (this.pageNo === this.maxPages) {
      return;
    }
    this.pageNo++;
    this.displayUsers = this.getPageData();
  }

  onPreviousPage(): void {
    if (this.pageNo == 1) {
      return;
    }
    this.pageNo--;
    this.displayUsers = this.getPageData();
  }

  onSetRowsToDisplay() {
    if (this.rows <= 0) {
      this.rows = 5;
    }
    this.pageNo = 1;
    this.maxPages = Math.ceil(this.users.length / this.rows);
    if (this.maxPages > 1) {
      this.displayUsers = this.getPageData();
    } else {
      this.displayUsers = this.users;
    }
  }
}
