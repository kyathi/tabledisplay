import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { SpecializedusersComponent } from "./specializedusers.component";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";

describe("SpecializedusersComponent", () => {
  let component: SpecializedusersComponent;
  let fixture: ComponentFixture<SpecializedusersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [SpecializedusersComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecializedusersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
