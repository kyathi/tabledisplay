import { Injectable } from "@angular/core";
import { userData } from "./Data/MockUsers";
import { user } from "./Models/user";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class UserProfileService {
  constructor(private http: HttpClient) {}

  getUserData(): user[] {
    return userData;
  }

  postStatusForUser(id: number, status: string): Observable<any> {
    return this.http.post("/api/", { id: id, status: status });
  }
}
