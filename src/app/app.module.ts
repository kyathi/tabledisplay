import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { UsersComponent } from "./users/users.component";
import { SpecializedusersComponent } from "./specializedusers/specializedusers.component";

const appRoutes: Routes = [
  { path: "customview", component: UsersComponent },
  { path: "fullview", component: SpecializedusersComponent }
];

@NgModule({
  declarations: [AppComponent, UsersComponent, SpecializedusersComponent],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
