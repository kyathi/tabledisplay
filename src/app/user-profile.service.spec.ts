import { TestBed } from "@angular/core/testing";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";

import { UserProfileService } from "./user-profile.service";

describe("UserProfileService", () => {
  let service: UserProfileService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserProfileService]
    });
    service = TestBed.inject(UserProfileService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("data should be returned", () => {
    expect(service.getUserData().length).toBeGreaterThanOrEqual(100);
  });
});
